module.exports = (sequelize, DataTypes) => {
  var AdmPar = sequelize.define('adm_par', {
    adm_id: { type: DataTypes.INTEGER, primaryKey: true },
    properties: DataTypes.TEXT
  })

  return AdmPar
}

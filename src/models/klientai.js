module.exports = (sequelize, DataTypes) => {
  var Client = sequelize.define('klientai', {
    klient_id: { type: DataTypes.INTEGER, primaryKey: true },
    klientas: DataTypes.STRING(70),
    moketojas: DataTypes.STRING(70),
    adresas: DataTypes.STRING(70),
    salis: DataTypes.STRING(60),
    miestas: DataTypes.STRING(30),
    indeksas: DataTypes.STRING(10),
    telefonai: DataTypes.STRING(60),
    fax: DataTypes.STRING(30),
    e_mail: DataTypes.STRING(100),
    kl_tipas: DataTypes.STRING(30),
    marsrutas: DataTypes.STRING(30),
    no_check: DataTypes.INTEGER(1),
    tab_nr: DataTypes.INTEGER,
    kainos_nr: DataTypes.DECIMAL(2, 0),
    e_klientas: DataTypes.DECIMAL(1, 0),
    dienos: DataTypes.DECIMAL(3, 0),
    max_skola: DataTypes.DECIMAL(12, 2),
    pvm_kodas: DataTypes.STRING(30),
    tiekejas: DataTypes.DECIMAL(1, 0),
    gavejas: DataTypes.DECIMAL(1, 0),
    sistema: DataTypes.STRING(70),
    dienos: DataTypes.DECIMAL(3, 0),
    cartsize: DataTypes.DECIMAL(15, 5)
  })

  Client.associate = function (models) {
    models.klientai.belongsTo(models.marsrut, {
      foreignKey: 'marsrutas',
      constraints: false,
      targetKey: 'kodas'
    })

    models.klientai.belongsTo(models.klientai, {
      foreignKey: 'moketojas',
      constraints: false,
      targetKey: 'klientas',
      as: 'Moketojai'
    })
  }

  return Client
}

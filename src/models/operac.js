module.exports = (sequelize, DataTypes) => {
  const { STRING, DATE, INTEGER, DECIMAL, TEXT } = DataTypes

  var Operac = sequelize.define('operac', {
    oper_id: { type: INTEGER, primaryKey: true },
    operacija: STRING(60),
    oper_apm: STRING(10)
  })

  return Operac
}

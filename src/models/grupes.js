module.exports = (sequelize, DataTypes) => {
  var Group = sequelize.define('grupes', {
    grup_id: { type: DataTypes.INTEGER, primaryKey: true },
    grupe: DataTypes.STRING(60)
  })

  return Group
}

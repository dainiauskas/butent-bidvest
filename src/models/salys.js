module.exports = (sequelize, DataTypes) => {
  var Country = sequelize.define('salys', {
    salis: { type: DataTypes.STRING(60), primaryKey: true },
    kodas: DataTypes.STRING(10),
    sal_id: DataTypes.INTEGER
  })

  return Country
}

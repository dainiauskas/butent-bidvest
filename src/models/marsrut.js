module.exports = (sequelize, DataTypes) => {
  var Route = sequelize.define('marsrut', {
    kodas: { type: DataTypes.STRING(20), primaryKey: true },
    dienos: DataTypes.STRING(30),
    laikas: DataTypes.STRING(15),
    mars_id: DataTypes.INTEGER
  })

  return Route
}

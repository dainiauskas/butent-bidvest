module.exports = (sequelize, DataTypes) => {
  const { STRING, DATE, INTEGER, DECIMAL, TEXT } = DataTypes

  var Discount = sequelize.define('nuolaid', {
    klientas: { type: STRING(60), primaryKey: true },
    tipas: STRING(30),
    grupe: STRING(60),
    preke: INTEGER,
    data_nuo: DATE,
    data_iki: DATE,
    kaina: DECIMAL(15, 5),
    procentas: DECIMAL(8, 3)
  })

  return Discount
}

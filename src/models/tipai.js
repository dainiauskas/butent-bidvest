module.exports = (sequelize, DataTypes) => {
  var Types = sequelize.define('tipai', {
    tipas: { type: DataTypes.STRING(60), primaryKey: true },
    tip_id: DataTypes.INTEGER
  })

  return Types
}

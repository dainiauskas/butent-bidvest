module.exports = (sequelize, DataTypes) => {
  var Good = sequelize.define('prekes', {
    prek_id: DataTypes.INTEGER,
    pavad: DataTypes.STRING,
    preke: { type: DataTypes.INTEGER, primaryKey: true },
    grupe: DataTypes.STRING(60),
    artikulas: DataTypes.STRING(30),
    pard_kaina: DataTypes.DECIMAL(15, 5),
    mato_vien: DataTypes.STRING(10),
    orig_pak: DataTypes.DECIMAL(6, 0),
    pak_kiek: DataTypes.DECIMAL(12, 5),
    no_check: DataTypes.INTEGER(1),
    savikaina: DataTypes.DECIMAL(15, 5),
    kaina_1: DataTypes.DECIMAL(15, 5),
    kaina_2: DataTypes.DECIMAL(15, 5),
    kaina_3: DataTypes.DECIMAL(15, 5),
    kaina_4: DataTypes.DECIMAL(15, 5),
    kaina_5: DataTypes.DECIMAL(15, 5),
    kaina_6: DataTypes.DECIMAL(15, 5),
    kaina_7: DataTypes.DECIMAL(15, 5),
    kaina_8: DataTypes.DECIMAL(15, 5),
    kaina_9: DataTypes.DECIMAL(15, 5),
    kaina_10: DataTypes.DECIMAL(15, 5),
    sertifikat: DataTypes.STRING(30),
    tiekejas: DataTypes.STRING(70),
    tiek_art: DataTypes.STRING(30),
    vieta: DataTypes.STRING(30),
    max_nuol: DataTypes.DECIMAL(7, 3),
    didm_antk: DataTypes.DECIMAL(10, 3),
    aprasymas: DataTypes.STRING(100),
    ch_b2b: DataTypes.DECIMAL(1,0)
  })

  Good.associate = function (models) {
    models.prekes.belongsTo(models.tipai, {
      foreignKey: 'tipas',
      constraints: false,
      sourceKey: 'tipas'
    })

    models.prekes.belongsTo(models.grupes, {
      foreignKey: 'grupe',
      constraints: false,
      targetKey: 'grupe',
      as: 'Groups'
    })

    models.prekes.belongsTo(models.klientai, {
      foreignKey: 'tiekejas',
      constraints: false,
      targetKey: 'klientas',
      as: 'Tiekejai'
    })

    models.prekes.belongsTo(models.salys, {
      foreignKey: 'kilm_salis',
      constraints: false
    })

    models.prekes.hasMany(models.likuciai, {
      foreignKey: 'preke',
      as: 'likuciai'
    })

    models.prekes.hasMany(models.apyv_gr, {
      foreignKey: 'preke',
      as: 'apyvGr'
    })

  }

  return Good
}

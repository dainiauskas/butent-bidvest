module.exports = (sequelize, DataTypes) => {
  var Item = sequelize.define('apyv_gr', {
    a_gr_id: { type: DataTypes.INTEGER, primaryKey: true },
    apyv_id: DataTypes.INTEGER,
    preke: DataTypes.INTEGER,
    artikulas: DataTypes.STRING(30),
    kiekis: DataTypes.DECIMAL(15, 5),
    mato_vien: DataTypes.STRING(10),
    kaina: DataTypes.DECIMAL(15, 5),
    valiuta: { type: DataTypes.STRING(5), defaultValue: 'EUR' },
    pvm_stat: { type: DataTypes.STRING(1), defaultValue: 'S' },
    pvm: { type: DataTypes.DECIMAL(10, 2), defaultValue: 21 },
    pvm_p_md: { type: DataTypes.STRING(1), defaultValue: '%' },
    pastaba: DataTypes.STRING(30)
  }, {
    tableName: 'apyv_gr',
    freezeTableName: true,
    hooks: {
      beforeCreate: function (item) {
        return Item.max('a_gr_id').then(max => {
          max++
          item.setDataValue('a_gr_id', max)
          return item
        })
      }
    }
  })

  Item.associate = function (models) {
    models.apyv_gr.belongsTo(models.prekes, {
      foreignKey: 'preke',
      constraints: false,
      sourceKey: 'preke',
      as: 'Prekes'
    })

    models.apyv_gr.belongsTo(models.apyvarta, {
      foreignKey: 'apyv_id',
      constraints: false,
    })
  }

  return Item
}

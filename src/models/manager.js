module.exports = (sequelize, DataTypes) => {
  var Manager = sequelize.define('manager', {
    klientas: { type: DataTypes.STRING(70), allowNull: false },
    data: DataTypes.DATE,
    tab_nr: DataTypes.INTEGER,
    manag_id: { type: DataTypes.INTEGER, primaryKey: true }
  })

  return Manager
}

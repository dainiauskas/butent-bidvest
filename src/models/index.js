const env = process.env.NODE_ENV
const config = require(`../../config/${env.trim()}`)

var fs = require('fs')
var path = require('path')
var Sequelize = require('sequelize')

var basename = path.basename(__filename)
var db = {}

const { db: { name, user, pass, options } } = config
const sequelize = new Sequelize(name, user, pass, options)

sequelize.authenticate().then(() => {
  console.log('Database connected and authenticated!')
  return true
}).catch(error => {
  console.error('Failed to connect and authenticate', error)
  return false
})

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
  })
  .forEach(file => {
    var model = sequelize['import'](path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db

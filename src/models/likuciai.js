module.exports = (sequelize, DataTypes) => {
  var Stock = sequelize.define('likuciai', {
    preke: DataTypes.INTEGER,
    data: DataTypes.DATE,
    sandelis: DataTypes.STRING(5),
    kiekis: DataTypes.DECIMAL(15, 5),
    savikaina: DataTypes.DECIMAL(15, 5),
    kaina: DataTypes.DECIMAL(15, 5),
    sumSavikaina: {
      type: DataTypes.VIRTUAL((DataTypes.DECIMAL(15, 5))),
      get: function () {
        return this.getDataValue('kiekis') * this.getDataValue('savikaina')
      }
    }
  })

  Stock.removeAttribute('id')

  Stock.associate = function (models) {
    models.likuciai.belongsTo(models.prekes, {
      foreignKey: 'preke',
      as: 'prekes'
    })

    models.likuciai.belongsTo(models.apyv_gr, {
      foreignKey: 'preke',
      targetKey: 'preke',
      constraints: false
    })
  }

  return Stock
}

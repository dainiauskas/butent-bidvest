module.exports = (sequelize, DataTypes) => {
  const { STRING, DATE, INTEGER, DECIMAL, TEXT } = DataTypes

  var Document = sequelize.define('apyvarta', {
    apyv_id: { type: INTEGER, primaryKey: true },
    raide: STRING(1),
    data: DATE,
    terminas: DATE,
    dok_serija: STRING(10),
    dokumentas: STRING(30),
    kitas_dok: STRING(30),
    kitas_dok2: STRING(30),
    operacija: STRING(60),
    tiek_sand: STRING(5),
    gavejas: STRING(70),
    uzsakymas: { type: DECIMAL(1, 0), defaultValue: 1 },
    pajamos: { type: DECIMAL(1, 0), defaultValue: 0 },
    pastabos: TEXT,
    pastabos_2: TEXT,
    isaf: { type: DECIMAL(1, 0), defaultValue: 1 },
    manager: INTEGER(4),
    mars_kod: STRING(30),
    viso: DECIMAL(15, 5),
    viso_val: STRING(5),
    pvm_suma: DECIMAL(15, 5),
    viso_1: DECIMAL(15, 5),
    viso_2: DECIMAL(15, 5),
    viso_3: DECIMAL(15, 5),
    viso_4: DECIMAL(15, 5),
    viso_5: DECIMAL(15, 5),
    skola_w: DECIMAL(15, 5)
  }, {
    hooks: {
      beforeCreate: function (document) {
        return Document.max('apyv_id').then(max => {
          max++
          document.setDataValue('apyv_id', max)
          return document
        })
      }
    }
  })

  Document.associate = function (models) {
    models.apyvarta.belongsTo(models.klientai, {
      foreignKey: 'gavejas',
      constraints: false,
      targetKey: 'klientas',
      as: 'Customer'
    })

    models.apyvarta.belongsTo(models.operac, {
      foreignKey: 'operacija',
      constraints: false,
      targetKey: 'operacija',
      as: 'Operac'
    })

    models.apyvarta.hasMany(models.apyv_gr, {
      foreignKey: 'apyv_id'
    })
  }

  return Document
}

require('dotenv').config()

const env = process.env.NODE_ENV
const config = require(`../config/${env.trim()}`)

const Glue = require('glue')

require('./plugins').config(config)

console.log('Port:', config.server.port)

const manifest = {
  server: config.server,
  register: config.register
}

const options = {
  relativeTo: __dirname
}

const add = function (x, y) {
    return x + y;
}

exports.deployment = (start) => {
  return Glue.compose(manifest, options)
    .then(async (server) => {
      await server.initialize()
        .then(server => {
          console.log('Initalize server...')
          return server
        })

      if (start) {
        require('./functions').loadFunctions(server)
        server.start((error, srv) => {
          console.error(error)
          process.exit(1)
        })
          .then(() => {
            console.log('Server running at: ', server.info.uri)
          })
      }

      return server
    })
}

if (!module.parent) {
  exports.deployment(true).catch(console.error)
}

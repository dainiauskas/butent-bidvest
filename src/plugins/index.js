const fs = require('fs')
const path = require('path')

const __defaultView = 'view'

module.exports.config = (cfg) => {
  let plugins = cfg.register.plugins

  fs.readdirSync(__dirname)
    .filter(item => {
      return (item.indexOf('.') !== 0) && (item.slice(-3) !== '.js')
    })
    .forEach(dirname => {
      let reltativeTo = path.join(__dirname, dirname)
      plugins.push({
        plugin: path.join(__dirname, dirname),
        routes: { prefix: '/api' }
      })
    })
}

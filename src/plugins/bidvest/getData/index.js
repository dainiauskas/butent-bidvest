const dotenv = require('dotenv')
const Boom = require('boom')

const env = process.env.NODE_ENV
const config = require(`../../../../config/${env.trim()}`)

const FtpClient = require('ftp')
const hbs = require('handlebars')
const fs = require('fs')
const moment = require('moment')

const { removeBrackets } = require('../../../functions/strings')

const Template = require('../../../functions/templates')

const knex = require('knex')(config.knex)

const getProducts = require('./Product')
const { getSuppliers, getCustomers } = require('./Clients')
const getDocuments = require('./Documents')

const products = [
  'Uom', 'Brand', 'ProductSalesCategory', 'ProductLine', 'ProductItem', 'SupplierProductItem', 'StockItem', 'StockUpdate'
]
const clients = [ 'Customer', 'DeliveryAddress' ]
const documents = ['Invoice', 'ProcessedOrder', 'CreditNote']

async function getParameters() {
  return await knex('adm_par').select('properties').first()
    .then(data => {
      const buf = Buffer.from(data.properties)
      return dotenv.parse(buf)
    })
}

let checkSand
getParameters()
  .then(param => {
    checkSand = removeBrackets(param.check_qty_sand).split(';')
  })

function dataToXML (obj, request) {
  const template = new Template(request.server.app.bidvest.templateTo)

  if (!obj.name) {
    throw Error(obj)
  }

  xml = template.compile(obj.name, obj.data)
  sendToFtp(obj.name, xml)

  return xml
}

function sendToFtp (name, content) {
  var ftp = new FtpClient()

  console.log("Connecting to FTP")

  ftp.on('ready', () => {
    console.log("FTP ready...")

    const fileName = name.concat('ListSubmit_', moment().format('YYYYMMDDHHmmssSSS'), '.xml')

    const path = config.ftp.path

    ftp.put(content, path + fileName, error => {
      if (error) {
        console.error(error)
      }

      ftp.end()
    })
    console.log(new Date(), ' File ', fileName, ' send to FTP')
  })

  ftp.on('error', error => {
    console.error('FTP: ', error, fileName)
    ftp.end()
  })

  if (config.ftp) {
    ftp.connect(config.ftp)
  }
}

async function task (request, h) {
  let data
  const params = request.params.file
  const template = new Template(request.server.app.bidvest.templateTo)


  if (!params) {
    return Boom.badRequest('No parameters given')
  }

  let limit = null
  if (request.query && request.query.limit) {
    limit = request.query.limit
  }

  let product = products.find(element => element.toLowerCase() === params.toLowerCase())
  if (typeof product !== 'string') {
    product = ''
  }

  let client = clients.find(element => element.toLowerCase() === params.toLowerCase())
  if (typeof client !== 'string') {
    client = ''
  }

  let document = documents.find(element => element.toLowerCase() === params.toLowerCase())
  if (typeof document !== 'string') {
    document = ''
  }

  switch (params.toLowerCase()) {
    case 'supplier':
      data = await getSuppliers(limit).then(obj => template.compile(obj.name, obj.data))
      break
    case document.toLowerCase():
      data = await getDocuments(document, request.payload, limit).then(obj => template.compile(obj.name, obj.data))
      break
    case product.toLowerCase():
      data = await getProducts(checkSand, product, limit).then(obj => template.compile(obj.name, obj.data))
      break
    case client.toLowerCase():
      data = await getCustomers(client, limit).then(obj => template.compile(obj.name, obj.data))
      break
    default:
      xml = ''
  }

  return h.response(data).type('text/xml')
}

function taskMin15 (request, reply) {
  getProducts(checkSand, 'StockUpdate').then(obj => dataToXML(obj, request))
  getDocuments('ProcessedOrder', request.payload).then(obj => dataToXML(obj, request))

  return { status: "Task Completed" }
}

function taskInvoice (request, reply) {
  getDocuments('Invoice', request.payload).then(obj => dataToXML(obj, request))

  return { status: "Task Completed" }
}

function taskDaily (request, reply) {
  const { getByName } = require('../../../functions/objects')

  getProducts(checkSand, 'All').then(res => {
    products.map(item => {
      getByName(res, item).then(obj => dataToXML(obj, request))
    })
  })

  getSuppliers().then(obj => dataToXML(obj, request))

  getCustomers('Customer').then(obj => dataToXML(obj, request))
  getCustomers('DeliveryAddress').then(obj => dataToXML(obj, request))

  // getDocuments('Invoice', request.payload).then(obj => dataToXML(obj, request))
  getDocuments('CreditNote', request.payload).then(obj => dataToXML(obj, request))

  return { status: "Task Completed" }
}

module.exports = { task, taskDaily, taskMin15, taskInvoice }

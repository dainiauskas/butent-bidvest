const env = process.env.NODE_ENV
const config = require(`../../../../config/${env.trim()}`)
const knex = require('knex')(config.knex)

const Sequelize = require('sequelize')
const Op = Sequelize.Op;

const models = require('../../../models')
const moment = require('moment')

const getQueryConfig = () => {
  return {
       where: {
         viso_5: { [Op.ne]: null }
       },
       include: [
         { model: models.klientai, as: 'Customer' },
         { model: models.apyv_gr, include: [ { model: models.prekes, as: 'Prekes' } ] }
       ]
  }
}

const getDocuments = async (name, params, limit) => {
  const query = getQueryConfig()

  // var hours = 21
  if (name === 'Invoice' || name === 'CreditNote') {
    // hours = 27
    query.where.ivestas = 1
    query.where.operacija = { [Op.like]: config.butenta.sales }
    query.where.dok_serija = { [Op.ne]: null }
    // query.where.kitas_dok = { [Op.ne]: null }

    if (name === 'CreditNote') {
      query.where.operacija = config.butenta.return
    }

  }

  if (name === 'Invoice' || name === 'ProcessedOrder') {
    query.where.gavejas = { [Op.ne]: null }
  }

  if (name === 'ProcessedOrder') {
    query.where.dok_serija = { [Op.eq]: null }
    query.where.kitas_dok = { [Op.eq]: null }
    query.where.uzsakymas = 1
  }

  if (params) {
    let { fromDate, toDate } = params
    clauseDateFrom = moment(fromDate).format('YYYY-MM-DD')
    clauseDateTo = moment(toDate).add(1, 'days').format('YYYY-MM-DD')
  } else if (name === 'Invoice') {
    clauseDateFrom = moment().format('YYYY-MM-DD')
    clauseDateTo = moment().add(1, 'days').format('YYYY-MM-DD')
  } else {
    clauseDateFrom = moment().add(1, 'days').format('YYYY-MM-DD')
    clauseDateTo = moment().add(2, 'days').format('YYYY-MM-DD')
  }

  query.where.data = {
    [Op.gte]: clauseDateFrom,
    [Op.lt]: clauseDateTo
  }

  return models.apyvarta.findAll(query)
    .map(el => el.get({ plain: true }))
    .map(el => {
      el.NoOfItems = el.apyv_grs.length
      return el
    })
    .then(document => {
      if (limit) document = document.filter((obj, i) => (limit > i))

      return { name, data: document }
    })
    .catch(error => console.error(error))
}

module.exports = getDocuments

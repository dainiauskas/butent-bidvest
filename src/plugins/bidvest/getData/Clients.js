const models = require('../../../models')
const { stringToWeeklyPatern } = require('../../../functions/strings')
const { getByName } = require('../../../functions/objects')

async function getSuppliers (whLimit) {
  let data = []

  let limit = null
  if (whLimit) {
    limit = Number(whLimit)
  }

  await models.klientai.findAll({
    where: {
      tiekejas: 1, e_klientas: 1
    },
    limit
  })
    .map(el => el.get({ plain: true }))
    .then(client => {
      data = { name: 'Supplier', data: client }
    })
    .catch(error => {
      console.error(error)
    })

  return data
}

// Todo: Filter by client ID
async function getCustomers (file, limit) {
  let data = []

  await models.klientai.findAll({
    where: {
      gavejas: 1, e_klientas: 1
    },
    include: [
      { model: models.marsrut, attributes: ['dienos', 'laikas'] },
      { model: models.klientai, as: 'Moketojai', attributes: ['klient_id'] }
    ]
  })
    .map(el => el.get({ plain: true }))
    .map(el => {
      if (!el.marsrut || !el.marsrut.dienos) {
        el.WeeklyPattern = 'x'.repeat(7)
      } else if (el.marsrut.dienos) {
        el.WeeklyPattern = stringToWeeklyPatern(el.marsrut.dienos)
      }

      return el
    })
    .then(client => {
      data.push({ name: 'Customer', data: client })
      data.push({ name: 'DeliveryAddress', data: client })
    })
    .catch(e => console.error(e))

  return getByName(data, file, limit)
}

module.exports = { getSuppliers, getCustomers }

const moment = require('moment')

const env = process.env.NODE_ENV
const config = require(`../../../../config/${env.trim()}`)

const knex = require('knex')(config.knex)

const { filterUniqueData, getByName } = require('../../../functions/objects')

/**
* @param {object} data Model Prekes data
*/
function getProductLine (data) {
  const line = data.map(item => {
    return { ...item.grupes, ...item.tipai }
  })

  const groups = [...new Set(line.map(JSON.stringify))].map(JSON.parse)
  return groups
}

/**
* @param {object} data Model Prekes data
*/
function getSupplierProducts (data) {
  const items = data.filter(item => {
    return (item.tiekejas && item.tiek_art)
  })

  return items
}

const updateUOMs = (e) => {
  let { pak_kiek, orig_pak } = e

  pak_kiek = Number(pak_kiek)
  orig_pak = Number(orig_pak)

  e.alterUOM     = null
  e.alterWeight  = 0
  e.alterRestr   = false

  if (pak_kiek > 0 && orig_pak === 0) {
    e.alterUOM     = 'dėž.'
    e.alterWeight  = pak_kiek
    e.alterRestr   = true
  } else if (pak_kiek === 0 && orig_pak > 0) {
    e.alterUOM     = 'dėž.'
    e.alterWeight  = orig_pak
    e.alterRestr   = false
  } else if (pak_kiek > 0 && orig_pak > 0) {
    e.alterUOM     = 'dėž.'
    e.alterWeight  = pak_kiek
    e.alterRestr   = true
  }

  return e
}

async function getProducts (checkSand, file, limit) {
  let res = []

  await knex('prekes')
    .select('prekes.preke', 'pavad', 'prekes.mato_vien', 'modelis', 'kilm_salis', 'pak_kiek', 'prekes.tiekejas', 'tiek_art',
      'grupes.grup_id', 'grupes.grupe',
      'kaina_1', 'kaina_2', 'kaina_3', 'kaina_4', 'kaina_5', 'kaina_6', 'kaina_7', 'kaina_8', 'kaina_9', 'kaina_10',
      'salys.kodas', 'tipai.tipas', 'tipai.tip_id', 'klientai.klient_id', 'sertifikat', 'pak_kiek', 'orig_pak', 'prekes.aprasymas', 'prekes.ch_b2b'
    )
    .join('grupes', 'prekes.grupe', 'grupes.grupe')
    .join('tipai', 'prekes.tipas', 'tipai.tipas')
    .leftJoin('salys', 'prekes.kilm_salis', 'salys.salis')
    .leftJoin('klientai', 'prekes.tiekejas', 'klientai.klientas')
    // .where('preke', 578)
    .whereNotNull('prekes.ch_b2b')
    .andWhere(function() {
      for (var i = 1; i <= 10; i++) {
        this.orWhere('kaina_' + i, 1112).orWhere('kaina_' + i, '<', 1111)
      }
    })
    .options({ nestTables: true })
    .map(el => ({ ...el }))
    .map(el => {
      let ret = { ...el.prekes, ...el }
      delete ret.prekes

      return updateUOMs(ret)
    })
    .filter(async el => {
      if (file === 'StockItem' || file === 'StockUpdate' || file === 'All') {
        el.likuciai = await knex('likuciai').select('savikaina', 'kaina', 'mato_vien', knex.raw('SUM(kiekis * savikaina) as sumSavikaina'))
          .max('data as maxData')
          .min('data as minData')
          .sum('kiekis as kiekis')
          .avg('savikaina as avgSavikaina')
          .where({ 'preke': el.preke })
          .whereIn('sandelis', checkSand)
          .then(res => ({ ...res[0] }))
          .catch(e => console.error(e))
      }

      for (var i = 1; i <= 10; i++) {
        if (el['kaina_' + i] === 1112) return true
      }

      let valid = false

      if (!el.likuciai) {
        valid = await knex('likuciai')
          .first('kiekis')
          .where('preke', el.preke)
          .where('kiekis', '>', 0)
          .whereIn('sandelis', checkSand)
          .then(res => ((res) ? 1 : 0))
          .catch(e => console.error(e))
      } else {
        valid = el.likuciai.kiekis > 0
      }

      if (valid) return true

      valid = await knex('apyv_gr').first('kiekis', 'apyvarta.operacija')
        .join('apyvarta', 'apyv_gr.apyv_id', 'apyvarta.apyv_id')
        .join('operac', 'apyvarta.operacija', 'operac.operacija')
        .where('preke', el.preke)
        .where('data', '>', moment().subtract(90, 'd').format('YYYY-MM-DD'))
        .whereNotNull('oper_isl')
        .whereNotNull('oper_pirk')
        .then(res => ((res) ? 1 : 0))
        .catch(e => console.error(e))

      return valid
    })
    .then(prekes => {
      res.push({ name: 'Uom', data: filterUniqueData(prekes, 'mato_vien') })
      res.push({ name: 'Brand', data: filterUniqueData(prekes, 'modelis') })
      res.push({ name: 'ProductSalesCategory', data: filterUniqueData(prekes, 'tipai') })
      res.push({ name: 'ProductLine', data: getProductLine(prekes) })
      res.push({ name: 'SupplierProductItem', data: getSupplierProducts(prekes) })
      res.push({ name: 'ProductItem', data: prekes })
      res.push({ name: 'StockItem', data: prekes })
      res.push({ name: 'StockUpdate', data: prekes })
    })
    .catch(e => console.error(e))

  if (file === 'All') {
    return res
  } else {
    return getByName(res, file, limit)
  }
}

module.exports = getProducts

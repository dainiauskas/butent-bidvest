const Boom = require('boom')
const Template = require('../../../functions/templates')
const models = require('../../../models')

const Sequelize = require('sequelize')
const Op = Sequelize.Op;

let moment = require('moment')
const xml2js = require('xml2js')

let parser = new xml2js.Parser({
  trim: true,
  explicitRoot: false
})

const priceField = (number) => {
  let field = 'pard_kaina'

  var x = Number(number)
  switch (true) {
    case (x === 0):
      field = 'pard_kaina'
      break
    case (x < 1):
      field = 'kaina'
      break
    case (x < 2):
      field = 'savikaina'
      break
    case (x < 3):
      field = 'mazm_kaina'
      break
    case (x < 4):
      field = 'didm_kaina'
      break
    case (x < 5):
      field = 'savik_2'
      break
    case (x < 17):
      field = 'kaina_' + (number - 5)
      break
    case (x < 18):
      field = 'kaina'
      break
    case (x < 19):
      field = 'kl_kaina'
      break
    case (x < 20):
      field = 'kaina_v'
      break
  }

  return field
}

const getClientDiscount = (item, client, moketojas) => {
  return models.nuolaid.findOne({ raw: true,
    where: {
      [Op.or]: {
        preke: item.preke,
        grupe: item.grupe,
        tipas: item.tipas
      },
      klientas: [client, moketojas],
      data_nuo: { [Op.or]: { [Op.lt]: moment().format(), [Op.eq]: null } },
      data_iki: { [Op.or]: { [Op.gt]: moment().format(), [Op.eq]: null } }
    }
  }).then(nuolaid => {
    if (!nuolaid) {
      return null
    }
    return { price: Number(nuolaid.kaina), percent: Number(nuolaid.procentas) }
  }).catch(error => {
    console.error(error)
    throw error
  })
}

const getPrekInfo = (item, client) => {
  if (!client) {
    return null
  }

  const fieldPrice = priceField(client.kainos_nr)

  return models.prekes.findByPk(item, { raw: true,
      attributes: [ 'preke', 'mato_vien', [ fieldPrice, 'price' ], 'orig_pak', 'tipas', 'grupe', 'vieta', 'max_nuol', 'didm_antk' ],
  }).then(async good => {
    if (!good) {
      return good
    }

    let price = good.price
    let klientas = client.sistema ? client.sistema : client.klientas
    let promotion = null
    let discount = await getClientDiscount(good, klientas, client.moketojas)

    if (client.kainos_nr == 11 && good.vieta && good.didm_antk) {
      promotion = moment().endOf('month').format()
      price = price - (price * (good.didm_antk / 100))
    } else if (discount) {
      if (discount.price > 0) {
        price = discount.price
      } else if (discount.percent > 0) {
        price = price - (price * (discount.percent / 100))
      }
    }

    if (price == 1111) price = null

    return { product: good.preke, uom: good.mato_vien, price: price, pack: good.orig_pak, promotion  }
  }).catch(error => {
    console.error(error);
    throw (error);
  })
}

const getPriceList = async (request, h) => {
  let data, content

  const template = new Template(request.server.app.bidvest.templateTo)

  let { strings } = request.server.methods

  if (!request.payload && (request.payload.content || request.payload.Content))
    return Boom.badData('Bad request payload')

  if (request.payload.content) {
    content = decodeURI(request.payload.content)
  } else if (request.payload.Content) {
    content = request.payload.Content
  } else {
    content = request.payload
  }

  if (!content) {
    return Boom.badData('Bad content request')
  }

  await parser.parseString(content, function (error, result) {
    if (error) {
      console.error('Error (params):', request.params)
      console.error('Error (payload):', content)
      console.error('Error (Parser): ', error)
      return Boom.badData('Bad data request', content)
    } else {
      data = result
    }
  })

  if (!data) {
    return Boom.badData('Bad data request', content)
  }

  console.log("PriceList TransactionID:", data.$.TransactionID)

  let good = await models.klientai.findByPk(data.$.AccountCode, { attributes: ['klientas', 'kainos_nr', 'sistema', 'moketojas'] })
     .then(client => Promise.all(data.ProductItemCode.map(item => getPrekInfo(item, client))))
     .filter(item => item != null)
     .catch(e => console.error(e))

  let resData = {
    ...data.$,
    good,
    ProductItemCount: good.length,
    EndTime: moment().format(),
    ErrorCount: data.ProductItemCode.length - good.length
  }

  return h.response(template.compile('CustomerAccountPriceListResponse', resData)).type('text/xml')
}

module.exports = getPriceList


module.exports = function routes (options) { 
  const getPriceList = require('./livePrices')
  const { addBaskets } = require('./Orders')
  const { taskDaily, taskMin15, taskInvoice, task } = require('./getData')
  const { getBalance, insertClaim } = require('./loyalty')

  let route = [
    { method: 'POST', path: '/bidvest/pricelist', handler: getPriceList, config: { payload: { parse: true } } },
    { method: 'POST', path: '/bidvest/baskets', handler: addBaskets },
    { method: 'GET', path: '/bidvest/tasks/{file?}', handler: task },
    { method: 'POST', path: '/bidvest/tasks/{file?}', handler: task },
    { method: 'GET', path: '/bidvest/tasks/day/{file?}', handler: taskDaily },
    { method: 'GET', path: '/bidvest/tasks/min15/{file?}', handler: taskMin15 },
    { method: 'GET', path: '/bidvest/tasks/invoice', handler: taskInvoice },
    { method: 'GET', path: '/bidvest/loyalty/getbalance', handler: getBalance},
    { method: 'POST', path: '/bidvest/loyalty/insertclaim', handler: insertClaim}
  ]

  return route
}

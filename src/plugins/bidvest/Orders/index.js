const dotenv = require('dotenv')
const env = process.env.NODE_ENV
const config = require(`../../../../config/${env.trim()}`)

const Sequelize = require('sequelize')
const Op = Sequelize.Op;

let moment = require('moment')
let models = require('../../../models')

const knex = require('knex')(config.knex)

const { removeBrackets } = require('../../../functions/strings')

async function getSkola(gavejas) {
  let skola = 0

  return await models.apyvarta.findAll({ raw: true, attributes: ["operacija", "skola_w", "terminas"],
      where: {
        gavejas,
        skola_w: { [Op.ne]: 0 },
        '$Operac.oper_apm$': { [Op.ne]: null },
        // terminas: { [Op.or]: { [Op.eq]: null, [Op.lt]: moment().format() } }
      },
      include: [
        { model: models.operac, as: 'Operac', attributes: ["oper_apm"] }
      ]
    })
    .map(row => {
      skola += Number(row.skola_w)
      return row
    })
    .then(data => {
      return skola
    })
    .catch(e => {
      console.error(e);
    })
}

function errorExist (data) {
  return data.filter(data => {
      return ['F', 'Q'].indexOf(data.statusCode)
    }).length > 0
}

function getManager (client) {
  return models.manager.findOne({
    attributes: [ 'tab_nr' ],
    where: {
      klientas: client
    },
    order: [[ 'data', 'DESC' ]],
    raw: true
  })
}

function createDocument (data, date) {
  let { orderId, accountCode, customerReference } = data

  return models.klientai
    .findByPk(accountCode, { attributes: [ ['klientas', 'gavejas'], "dienos", ['marsrutas', 'mars_kod'] ], raw: true,
      include: [
        { model: models.klientai, as: 'Moketojai', attributes: ["dienos"] }
      ]
    })
    .then(async (client) => {
      let manager = await getManager(client.gavejas)
      if (!manager) {
        tab_nr = null
      } else {
        tab_nr = manager.tab_nr
      }

      let dienos = null
      if (!client.dienos || client.dienos === 0) {
        dienos = client['Moketojai.dienos']
      } else {
        dienos = client.dienos
      }

      let terminas = null
      if (dienos && dienos > 0) {
        terminas = moment(date).add(dienos, 'days').format()
      }

      return {
        data: moment(date).format(),
        dokumentas: orderId,
        tiek_sand: config.butenta.warehouse,
        operacija: config.butenta.operation,
        kitas_dok2: customerReference,
        ...client,
        terminas,
        manager: tab_nr,
        pastabos_2: '_B2B_'
      }
    })
    .then(item => {
      return models.apyvarta.create(item)
    })
    .catch(error => {
      console.error(error)
      return error
    })
}

async function createDocItem (did, data) {
  let viso = 0
  let viso1 = 0
  let pvmSuma = 0

  for (var value of data) {
    await models.prekes.findAll({ where: { preke: value.productCode }, raw: true })
      .then(good => {
        let { productCode, uomCode, quantity, unitPrice, basketItemId } = value

        let { artikulas, orig_pak, pak_kiek, mato_vien } = good[0]

        orig_pak = Number(orig_pak)
        pak_kiek = Number(pak_kiek)

        if (mato_vien !== uomCode) {
          if (orig_pak > 0 && pak_kiek == 0) {
            quantity  = quantity * orig_pak
            unitPrice = unitPrice / orig_pak
            uomCode = mato_vien
          } else if (orig_pak >= 0 && pak_kiek > 0) {
            quantity  = quantity * pak_kiek
            unitPrice = unitPrice / pak_kiek
            uomCode = mato_vien
          }
        }

        return models.apyv_gr.create({
          apyv_id: did,
          preke: productCode,
          artikulas,
          mato_vien: uomCode,
          kiekis: quantity,
          kaina: unitPrice,
          pastaba: basketItemId
        })
      })
      .then(item => {
        let { kiekis, kaina, pvm } = item

        viso1 += (kiekis * kaina)
        viso = viso1 * ((pvm / 100) + 1)
        pvmSuma += viso - viso1

        return models.apyvarta.update(
          { viso, viso_val: 'EUR', pvm_suma: pvmSuma, viso_1: viso1, viso_3: viso1, viso_5: viso, skola_w: viso },
          { where: { apyv_id: did } }
        )
      })
      .catch(error => {
        console.error(error)
      })
  }
}

async function addBaskets (request, reply) {
  let config = null

  await models.adm_par.findOne({ order: [[ 'adm_id', 'DESC' ]], raw: true })
    .then(data => {
      const buf = Buffer.from(data.properties)
      config = dotenv.parse(buf)
    })

  const klientTip = removeBrackets(config.no_check_kl_tp)
  const prekGroup = removeBrackets(config.no_check_pr_gr).split(';')
  const checkSand = removeBrackets(config.check_qty_sand).split(';')

  // Validation
  let valid = { orders: [] }
  let errors = 0

  for (var value of request.payload.baskets) {
    let orderLine = []

    let client = await models.klientai.findByPk(value.basketHeader.accountCode, {
      raw: true,
      include: [
        { model: models.klientai, as: 'Moketojai', attributes: ["max_skola", "cartsize"] }
      ]
    })
    .then(data => {
      if (!data.max_skola && data['Moketojai.max_skola']) {
        data.max_skola = data['Moketojai.max_skola']
      }
      if (!data.cartsize && data['Moketojai.cartsize']) {
        data.cartsize = data['Moketojai.cartsize']
      }
      return data
    })
    .catch(e => console.error(e))

    let docTotal = 0;
    for (var item of value.basketItems) {
      await models.prekes.findOne({ where: { preke: item.productCode }, raw: true })
        .then(async function (good) {
          let status = []
          let { quantity, unitPrice } = item

          docTotal += (quantity * unitPrice) * ((21 / 100) + 1)

          orderLine.push({
            basketItemId: item.basketItemId,
            orderStatuses: status
          })

          if (good == null) {
            status.push({ statusCode: 'U', statusMessage: 'Product unavailable for order' })
            errors++
            return
          }

          if (!client.no_check || !good.no_check ||
            klientTip !== client.kl_tipas || prekGroup.indexOf(good.grupe) < 0) {
            await models.likuciai.sum('kiekis', {
              where: {
                preke: good.preke,
                kiekis: { [Op.gt]: 0 },
                sandelis: { [Op.in]: checkSand }
              },
              raw: true
            })
              .then(sum => {
                if (isNaN(sum) || sum < item.quantity) {
                  status.push({ statusCode: 'Q', statusMessage: 'Quantity request not available' })
                  errors++
                }
                console.log(`Date: ${moment().format("YYYY-MM-DD HH:mm:ss")}, Good: ${good.preke}, FQ: ${sum}, RQ: ${item.quantity}`)
                console.log(`   ---> Errors: ${errors}, Status: ${status}`)
              })
              .catch(e => console.error(e))
          }

          if (!errorExist(status)) {
            status.push({ statusCode: 'S', statusMessage: 'Order line valid' })
          }
        })
        .catch(e => console.error(e))
    }

    let statusCode = 'F'
    let statusMessage = 'Order validation failed'

    if (errors === 0 && client.cartsize && docTotal < client.cartsize) {
      let diff = client.cartsize - docTotal
      errors++
      statusCode = 'F'
      statusMessage = `Atsiprašome bet iki krepšelio sumos trūksta ${diff} EUR.`
    }

    if (errors === 0 && client.max_skola !== null && client.max_skola > 0) {
      let skola = await getSkola(client.klientas)
      let totalSkola = (skola + docTotal) - Number(client.max_skola)

      if (totalSkola > 0) {
        errors++
        statusCode = 'F'
        statusMessage = `Atsiprašome bet Jūsų užsakymas viršija kredito limitą ${totalSkola} EUR. Užsakymo priimti negalime.`
      }
    }

    if (errors === 0) {
      statusCode = 'S'
      statusMessage = 'Order submitted successfully'
    }

    var { basketHeaderId, orderId } = value.basketHeader

    valid.orders.push({
      orderHeader: {
        basketHeaderId,
        orderId,
        orderStatus: { statusCode, statusMessage }
      },
      orderLines: orderLine
    })
  }

  if (errors === 0) {
    for (value of valid.orders) {
      let { basketHeaderId } = value.orderHeader

      let orderOne = request.payload.baskets.filter(data => {
        return data.basketHeader.basketHeaderId === basketHeaderId
      })

      createDocument(orderOne[0].basketHeader, orderOne[0].basketItems[0].deliveryDate)
        .then(doc => {
          createDocItem(doc.apyv_id, orderOne[0].basketItems)
        })
    }
  }

  return valid
}

module.exports = { addBaskets }

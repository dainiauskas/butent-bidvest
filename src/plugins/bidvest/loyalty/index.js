
const env = process.env.NODE_ENV

const Boom = require('boom')
const Template = require('../../../functions/templates')
const models = require('../../../models')

const config = require(`../../../../config/${env.trim()}`)
const knex = require('knex')(config.knex)

let moment = require('moment')
const xml2js = require('xml2js')

let parser = new xml2js.Parser({
    trim: true,
    explicitRoot: false
})

async function getPoints(clientID) {
    var client

    return await knex("klientai").first("klientas", "moketojas").where("klient_id", clientID)
    .then((data) => {
        client = (data.moketojas == null) ? data.klientas : data.moketojas
    }).then(() => {
        return knex("point_count").where("klientas", client).where("cleared", null)
            .sum("total as LoyaltyTotal").first()
    }).then(total => loyaltyTotal = total.LoyaltyTotal)
    .then(() => {
        return knex("point_count").where("klientas", client).where("cleared", null).where("usable", 1)
        .sum("total as LoyaltyAvailableToClaim").first()
    }).then(total => loyaltyAvailableToClaim = total.LoyaltyAvailableToClaim)
    .then(() => { 
        return { "CustomerAccountNumber": clientID, 
            "LoyaltyTotal": loyaltyTotal,
            "LoyaltyAvailableToClaim": loyaltyAvailableToClaim,
        }
    })
}

const getBalance = async (request, h) => {
    let content, clientID, client, loyaltyTotal, loyaltyAvailableToClaim

    let data = request.query.accountcode

    if (!data) {
        return Boom.badData('Bad data request', content)
    }

    clientID = data

    let result = await getPoints(clientID)

    const template = new Template(request.server.app.bidvest.templateTo)

    return h.response(template.compile('LoyaltyGetBalance', result)).type('text/xml')
}

const insertClaim = async (request, h) => {
    let data

    if (!request.payload && (request.payload.content || request.payload.Content))
        return Boom.badData('Bad request payload')

    if (request.payload.content) {
        content = decodeURI(request.payload.content)
    } else if (request.payload.Content) {
        content = request.payload.Content
    } else {
        content = request.payload
    }
        
    if (!content) {
    return Boom.badData('Bad content request')
    }

    await parser.parseString(content, function (error, result) {
        if (error) {
            console.error('Error (params):', request.params)
            console.error('Error (payload):', content)
            console.error('Error (Parser): ', error)
            return Boom.badData('Bad data request', content)
        } else {
            data = result
        }
    })

    if (!data) {
        return Boom.badData('Bad data request', content)
    }

    const { CustomerAccountCode } = data.ClaimHeader[0]
    const { Comments, EnteredDate } = data.ClaimSummary[0]
    const details = data.ClaimDetails[0].Voucher

    const { klientas, moketojas} = await knex("klientai").where("klient_id", CustomerAccountCode).first("klientas", "moketojas")
        .then(cl => { 
            return cl
        })
        .catch(e => console.error(e))

    let new_id, status = false

    await knex("apyvarta").max("apyv_id as id").then(r => {return r[0].id})
        .then(async (id) => {
            new_id = id+1

            status = await knex.insert({
                "apyv_id": id+1,
                "data": EnteredDate,
                "dokumentas": Comments,
                "pajamos": false,
                "uzsakymas": true,
                "operacija": "Lojalumas",
                "gavejas": klientas,
                "tiek_sand": "S6",
            }).into('apyvarta')
            .then(async () => {
                for (var value of details) {
                    await knex("apyv_gr").max("a_gr_id as id")
                    .then(r => {
                        return r[0].id+1
                    })
                    .then(async (gr_id) => {
                        let good = await knex("prekes").where("preke", value.VoucherCode).first()
                        
                        let item = {
                            "preke": value.VoucherCode,
                            "artikulas": good.artikuls,
                            "mato_vien": good.mato_vien,
                            "kiekis": value.Quantity,
                            "apyv_id": new_id,
                            "a_gr_id": gr_id,
                        }

                        await knex.insert(item).into("apyv_gr")

                        return { item, good }
                    }).then(async (data) => {
                        console.log(data)
                        if (!data.good.points) {
                            return
                        }

                        await knex("point_count").max("pc_id as id").then(r => r[0].id+1)
                        .then(async (pc_id) => {
                            await knex.insert({
                                "pc_id": pc_id,
                                "apyv_id": data.item.apyv_id,
                                "a_gr_id": data.item.a_gr_id,
                                "data": EnteredDate,
                                "klientas": moketojas,
                                "preke": data.item.preke,
                                "points": data.good.points,
                                "total": data.good.points*data.item.kiekis,
                            }).into("point_count")
                            .catch(e => console.log(e))
                        }).catch(e => console.log(e))
                    })
                }
            })
            .then(() => {
                return true
            })
            .catch(e => console.log(e))
        }).catch(e => console.log(e))
    
    let result = await getPoints(CustomerAccountCode)

    const template = new Template(request.server.app.bidvest.templateTo)

    return h.response(template.compile('LoyaltyGetBalance', result)).type('text/xml')
}

module.exports = { getBalance, insertClaim }

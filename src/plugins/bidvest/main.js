const path = require('path')
const pjson = require('./package.json')
const routes = require('./routes')

exports.plugin = {
  pkg: require('./package.json'),
  register: (server, options) => {
    server.app[pjson.name] = { templateTo: __dirname }

    server.route(routes(options))
  }
}

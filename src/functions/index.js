const Strings = require('./strings')

module.exports.loadFunctions = (server) => {
  Object.keys(Strings).forEach(key => {
    server.method('strings.' + key, Strings[key], {})
  })
}

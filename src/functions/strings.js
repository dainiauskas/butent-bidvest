const removeBrackets = (value) => {
  return value.replace('[', '').replace(']', '')
}

const stringToWeeklyPatern = (data) => {
  if (!data) data = "8"

  let tpl = ['S', 'M', 'T', 'W', 'T', 'F', 'S']
  let m = data.replace('7','0').split(',').map(Number)

  for (var i = 0; i < 7; i++) {
    if (m.indexOf(i) < 0) {
      tpl[i] = 'x'
    }
  }

  return tpl.join('')
}

module.exports = {
  removeBrackets,
  stringToWeeklyPatern
}

const filterUniqueData = (data, field) => {
  return [...new Set(data.map(item => item[field]).map(JSON.stringify))].map(JSON.parse).filter(key => key !== null)
}

const getByName = async (data, name, limit) => {
  if (name) {
    let ret = data.filter(obj => obj.name === name)
    if (limit) ret[0].data = ret[0].data.filter((obj, i) => (limit > i))

    return ret[0]
  } else {
    return data
  }
}

module.exports = {
  filterUniqueData,
  getByName
}

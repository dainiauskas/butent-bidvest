const path = require('path')
const fs = require('fs')
const hbs = require('handlebars')

class Template {
  constructor(setViewPath) {
    this.mainPath = setViewPath
    this.templates = 'views'
    this.extension = 'xml'
    this.name = ''
  }

  getTemplateDir () {
    return path.resolve(this.mainPath, this.templates, this.name + '.' + this.extension)
  }

  readTemplate () {
    return fs.readFileSync(this.getTemplateDir(), 'utf8', function (error, tpl) {
      if (error) throw error
      return tpl
    })
  }

  compile (templateName, objectData) {
    this.name = templateName

    const template = hbs.compile(this.readTemplate())
    return template(objectData)
  }
}

module.exports = Template

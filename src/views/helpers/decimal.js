function isNumeric (n) {
  return !isNaN(parseFloat(n)) && isFinite(n)
}

module.exports = (num, dec) => {
  if (!isNumeric(num)) num = 0
  if (!isNumeric(dec)) dec = 5

  return parseFloat(num).toFixed(dec)
}

let moment = require('moment')

module.exports = (dt) => {
  if (!dt) return null

  return moment(dt).format('YYYY-MM-DDTHH:mm:ss')
}

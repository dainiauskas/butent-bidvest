module.exports = (passedString, endString) => {
  if (passedString === null) return passedString
  if (endString === null) return passedString

  return passedString.substring( 0, endString )
}

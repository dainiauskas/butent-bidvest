module.exports = {
  apps : [{
    name      : "testB2B",
    script    : "./src/server.js",
    watch     : true,
    env       : {
      "PORT"      : 3000,
      "NODE_ENV"  : "development"
    },
    env_test : {
      "PORT"      : 41867,
      "NODE_ENV"  : "test"
    },
    env_production : {
      "PORT"      : 41867,
      "NODE_ENV"  : "production"
    }
  }]
}

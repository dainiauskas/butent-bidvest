const globalConfig = require('./global')

const { register, butenta } = globalConfig

const config = {
  server: {
    port: 23450
  },
  db: {
    name: 'db_nowaco',
    user: 'root',
    pass: 'chei4Jai',
    options: {
      host: '10.10.10.151',
      port: 3306,
      logging: false,
      dialect: 'mysql',
      timezone: '+03:00',
      dialectOptions: {
        charset: 'cp1257'
      },
      collate: 'cp1257_lithuanian_ci',
      define: {
        freezeTableName: true,
        freeTableName: true,
        timestamps: false
      }
    }
  },
  knex: {
    client: 'mysql2',
    debug: false,
    connection: {
      host: '10.10.10.151',
      user: 'root',
      password: 'chei4Jai',
      database: 'db_nowaco',
      charset: 'cp1257',
      connectTimeout: 90000
    }
  },
  ftp: {
    host: '10.10.10.210',
    user: 'BidOne',
    password: 'B1d0n3U4t',
    secure: false,
    secure: false,
    connTimeout: 20000,
    pasvTimeout: 20000,
    keepalive: 20000,
    path: '/Dataloads/'
  },
  register,
  butenta: {
    operation: 'Pardavimas bidOne',
    return: 'Grąžino pirkėjas',
    sales: 'Pard%',
    warehouse: 'S6',
    character: 'W'
  }
}

module.exports = config


const config = {
  register: {
    plugins: [
      {
        plugin: 'good',
        options: {
          ops: {
            interval: 1000
          },
          reporters: {
            myConsoleReporter: [{
              module: 'good-squeeze',
              name: 'Squeeze',
              args: [{ log: '*', response: '*' }]
            }, {
              module: 'good-console'
            }, 'stdout']
          }
        }
      },
      { plugin: 'vision',
        options: {
          engines: {
            xml: require('handlebars')
          },
          relativeTo: __dirname,
          path: '../src/views',
          helpersPath: '../src/views/helpers'
        }
      },
      { plugin: 'hapi-cron',
        options: {
          jobs: [{
            name: 'taskday',
            time: '0 50 23 * * *',
            timezone: 'Europe/Vilnius',
            request: {
              method: 'GET',
              url: '/api/bidvest/tasks/day'
            },
          }, {
            name: 'invoice',
            time: '0 0 10 * * *',
            timezone: 'Europe/Vilnius',
            request: {
              method: 'GET',
              url: '/api/bidvest/tasks/invoice'
            },
          }, {
            name: 'task15min',
            time: '0 */15 * * * *',
            timezone: 'Europe/Vilnius',
            request: {
              method: 'GET',
              url: '/api/bidvest/tasks/min15'
            },
            onComplete: (res) => {
              // console.log('Min15', res)
            }
          }]
        }
      }
    ]
  }
}

module.exports = config

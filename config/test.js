const globalConfig = require('./global')

const { register, butenta } = globalConfig

const config = {
  server: {
    port: 23451
  },
  db: {
    name: 'db_test',
    user: 'user_nowaco',
    pass: 'pwd_nowaco',
    options: {
      host: '10.10.10.154',
      port: 3306,
      logging: console.log,
      dialect: 'mysql',
      timezone: '+03:00',
      dialectOptions: {
        charset: 'cp1257'
      },
      collate: 'cp1257_lithuanian_ci',
      define: {
        freezeTableName: true,
        freeTableName: true,
        timestamps: false
      }
    }
  },
  knex: {
    client: 'mysql2',
    debug: false,
    connection: {
      host: '10.10.10.154',
      user: 'user_nowaco',
      password: 'pwd_nowaco',
      database: 'db_test',
      charset: 'cp1257',
      connectTimeout: 90000
    }
  },
  ftp: {
    host: '10.10.10.215',
    user: 'BDirectFTP',
    password: 'BD1r3ctFTPUs3r',
    secure: false,
    connTimeout: 20000,
    pasvTimeout: 20000,
    keepalive: 20000,
    path: '/Dataloads/'
  },
  register,
  butenta: {
    operation: 'Pardavimas bidOne',
    return: 'Grąžino pirkėjas',
    sales: 'Pard%',
    warehouse: 'S6',
    character: 'W'
  }
}

module.exports = config

const globalConfig = require('./global')

const { register, butenta } = globalConfig

const config = {
  server: {
    port: 23451
  },
  db: {
    name: 'db_nowlv',
    user: 'user_nowlv',
    pass: 'pwd_nowlv',
    options: {
      host: '10.10.10.151',
      port: 3306,
      logging: console.log,
      dialect: 'mysql',
      timezone: '+03:00',
      dialectOptions: {
        charset: 'cp1257'
      },
      collate: 'cp1257_lithuanian_ci',
      define: {
        freezeTableName: true,
        freeTableName: true,
        timestamps: false
      }
    }
  },
  knex: {
    client: 'mysql2',
    debug: false,
    connection: {
      host: '10.10.10.151',
      user: 'user_nowlv',
      password: 'pwd_nowlv',
      database: 'db_nowlv',
      charset: 'cp1257',
      connectTimeout: 90000
    }
  },
  ftp: {
    host: '10.10.10.215',
    user: 'BDirectFTP',
    password: 'BD1r3ctFTPUs3r',
    secure: false,
    connTimeout: 20000,
    pasvTimeout: 20000,
    keepalive: 20000,
    path: '/LV/Dataloads/'
  },
  register,
  butenta: {
    operation: 'Pardavimas bidOne',
    return: 'Grąžino pirkėjas (Atgriešana no pircēja)',
    sales: 'Pard%',
    warehouse: 'S6',
    character: 'W'
  }
}

module.exports = config
